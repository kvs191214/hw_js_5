
let fraction = (a, b) => a / b;

function check (num) {
    while (isNaN(num)) {
        num = +prompt('Введено не число.Введіть число');
    }
    return num
}

alert('Завдання 1 - частка чисел');

let num1 = check(+prompt('Введіть перше число'));

let num2 = check(+prompt('Введіть друге число'));

while (num2 === 0 || isNaN(num2)) {
        num2 = +prompt('Дільник не може дорівнювати нулю. Введіть друге число');
}


alert(fraction(num1, num2));

alert('Завдання 2 - математичні дії');

function calc (a, b, operation) {

    let result;

    if (operation === '+') {
        result = a + b;
    } else if (operation === '-') {
        result = a - b;
    } else if (operation === '*') {
        result = a * b;
    } else {
        result = a / b;
    }

    return result
}

let num3 = check(+prompt('Введіть перше число'));

let operation = prompt("Введіть дію ' +, -, *, /'");

while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
    operation = prompt("Невірна дія! Введіть дію ' +, -, *, /'");
}

let num4 = check(+prompt('Введіть друге число'));

if (operation === '/') {
    while (num4 === 0 || isNaN(num4)) {
        num4 = +prompt('Дільник не може дорівнювати нулю. Введіть друге число');
    }
}

alert(calc(num3, num4, operation));

alert('Завдання 3 - факторіал чісла');

let factorial = (a) => a ? a * factorial(a - 1) : 1;

let num5 = check(+prompt('Ведіть число'));

alert(num5 + '! = ' + factorial(num5));